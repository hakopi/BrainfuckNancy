﻿using System.Collections.Generic;
using System.Text;
using BrainfuckCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BrainfuckCoreTest
{
    [TestClass]
    public class HelloWorld
    {
        [TestMethod]
        public void HelloWorldTest()
        {
            string result = new Interpreter(100)
                .Load(
                    "+++++++++[>++++++++>+++++++++++>+++++<<<-]>.>++.+++++++..+++.>-.------------.< ++++++++.--------.++ +.------.--------.> +.")
                .Execute(string.Empty)
                .CharsToString();

            Assert.AreEqual("Hello, world!", result);
        }
    }

    public static class Extensions
    {
        /// <summary>
        /// chars を string に変換します
        /// http://stackoverflow.com/questions/11654190/ienumerablechar-to-string
        /// </summary>
        public static string CharsToString(this IEnumerable<char> chars)
        {
            var builder = new StringBuilder();

            foreach (var c in chars)
            {
                builder.Append(c);
            }

            return builder.ToString();
        }
    }
}
