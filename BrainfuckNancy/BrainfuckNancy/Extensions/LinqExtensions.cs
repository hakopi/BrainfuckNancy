﻿using System.Collections.Generic;
using System.Text;

namespace BrainfuckNancy.Extensions
{
    internal static class LinqExtensions
    {
        /// <summary>
        /// chars を string に変換します
        /// http://stackoverflow.com/questions/11654190/ienumerablechar-to-string
        /// </summary>
        public static string CharsToString(this IEnumerable<char> chars)
        {
            var builder = new StringBuilder();

            foreach (var c in chars)
            {
                builder.Append(c);
            }

            return builder.ToString();
        }
    }
}
