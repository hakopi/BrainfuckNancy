﻿using System.Collections.Generic;
using BrainfuckNancy.Models;

namespace BrainfuckNancy.Extensions
{
    internal static class ModelExtensions
    {
        /// <summary>
        /// Result をセットします
        /// </summary>
        public static ExecResult SetResult(this ExecResult execResult, string result)
        {
            execResult.Result = result;
            return execResult;
        }

        /// <summary>
        /// Output をセットします
        /// </summary>
        public static ExecResult SetOutput(this ExecResult execResult, string output)
        {
            execResult.Output = output;
            return execResult;
        }

        /// <summary>
        /// Output をセットします
        /// </summary>
        public static ExecResult SetOutput(this ExecResult execResult, IEnumerable<char> output)
        {
            return execResult.SetOutput(output.CharsToString());
        }
    }
}
