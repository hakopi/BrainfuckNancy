﻿using System.Collections.Generic;
using System.Linq;

namespace BrainfuckCore.Command
{
    internal static class SingletonCommands
    {
        public static readonly MoveNext MoveNext = new MoveNext();
        public static readonly MovePrevious MovePrevious = new MovePrevious();
        public static readonly Increment Increment = new Increment();
        public static readonly Decrement Decrement = new Decrement();
        public static readonly GetChar GetChar = new GetChar();
        public static readonly Read Read = new Read();
        public static readonly Open Open = new Open();
        public static readonly Close Close = new Close();

        private static readonly Command[] Commands = new Command[]
        {
            MoveNext,
            MovePrevious,
            Increment, 
            Decrement, 
            GetChar, 
            Read, 
            Open, 
            Close,  
        };

        /// <summary>
        /// 文字列を Command のシーケンスに変換します
        /// コマンドに変換できない文字は無視されます
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static IEnumerable<Command> ToCommands(this string code)
        {
            return string.IsNullOrEmpty(code) ? Enumerable.Empty<Command>() : code.ToCharArray().ToCommands();
        }

        /// <summary>
        /// char のシーケンスを Command のシーケンスに変換します
        /// コマンドに変換できなかったものは取り除かれます。
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        private static IEnumerable<Command> ToCommands(this IEnumerable<char> chars)
        {
            return chars.Select(ToCommand).Where(command => command != null);
        }

        /// <summary>
        /// 文字をコマンドに変換します。
        /// 変換できなかった場合は null を返します。
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private static Command ToCommand(char c)
        {
            return Commands.FirstOrDefault(command => command.Equals(c));
        }
    }
}
