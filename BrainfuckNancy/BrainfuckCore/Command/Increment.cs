﻿namespace BrainfuckCore.Command
{
    internal sealed class Increment: Command
    {
        private const char CharExp = '+';

        protected override char ToChar()
        {
            return CharExp;
        }

        public override char? Execute(Context context)
        {
            context.Memory.Increment();
            return null;
        }
    }
}
