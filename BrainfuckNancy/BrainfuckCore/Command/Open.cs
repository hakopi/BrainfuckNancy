﻿namespace BrainfuckCore.Command
{
    internal sealed class Open : Command
    {
        private const char CharExp = '[';

        protected override char ToChar()
        {
            return CharExp;
        }

        public override char? Execute(Context context)
        {
            if (context.Memory.CurrentValue == 0)
            {
                context.Commands.Jump(context.Commands.SearchNextClose());
            }
            return null;
        }
    }
}
