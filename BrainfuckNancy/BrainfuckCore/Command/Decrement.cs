﻿namespace BrainfuckCore.Command
{
    internal sealed class Decrement: Command
    {
        private const char CharExp = '-';

        protected override char ToChar()
        {
            return CharExp;
        }

        public override char? Execute(Context context)
        {
            context.Memory.Decrement();
            return null;
        }
    }
}
