﻿namespace BrainfuckCore.Exception
{
    public class OutOfMemoryException: BrainfuckException
    {
        internal OutOfMemoryException(int memorySize, int index)
            : base($"ポインタが不正な値になりました。メモリサイズ:{memorySize}, ポインタ:{index}")
        {
        }
    }
}
