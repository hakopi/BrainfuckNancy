﻿namespace BrainfuckCore.Exception
{
    /// <summary>
    /// Brainfuck 実行時の 例外
    /// </summary>
    public abstract class BrainfuckException: System.Exception
    {
        private const string DefaultMessage = "不明なエラー";

        protected BrainfuckException(string message)
            : base(message)
        {
        }

        protected BrainfuckException()
            : this(DefaultMessage)
        {
        }

        public string ToShortString()
        {
            return $"{this.GetType().Name}: {this.Message}";
        }
    }
}
