﻿namespace BrainfuckCore.Exception
{
    public class DestinationCloseNotFoundException: BrainfuckException
    {
        private const string DefaultMessage = "[ に対応する ] が見つかりませんでした。";

        internal DestinationCloseNotFoundException()
            :base(DefaultMessage)
        {
        }
    }
}
